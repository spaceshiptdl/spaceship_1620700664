﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private RectTransform winPanel;
        [SerializeField] private Text scoreText;
        [SerializeField] private Text levelText;
        [SerializeField] private GameManager gameManager;

        public static int score;
        private int level;
        

        private void Start()
        {
            winPanel.gameObject.SetActive(false);
            scoreText.GetComponent<Text>();
            levelText.GetComponent<Text>();
        }

        private void Update()
        {
            scoreText.text = "Score : " + score;
            levelText.text = "You Win : Level " + level;
            if (score >= 100 && score < 200)
            {
                winPanel.gameObject.SetActive(true);
                level = 1;
            }
            else if (score >= 200 && score < 300)
            {
                level = 2;
            }
            else if (score >= 300)
            {
                gameManager.Restart();
            }
        }
    }
}


