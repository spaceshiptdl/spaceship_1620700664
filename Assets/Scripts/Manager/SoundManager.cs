﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
   
    [SerializeField] private SoundClip[] soundClips;
    [SerializeField] private AudioSource audioSource;
    
    public static SoundManager Instance { get; private set; }
    [Serializable]
    public struct SoundClip
    {
        public Sound Sound;
        public AudioClip AudioClip;
    }
    
    public enum Sound
    {
        BGM,
        Fire,
        Destroy,
    }
    
    // Play Sound
    /// </summary>
    /// <param name="audioSource"></param>>
    /// <param name = "sound"</param>
    public void Play(AudioSource audioSource,Sound sound)
    {
        Debug.Assert(audioSource != null,"audioSource cannot be null");

        audioSource.clip = GetAudioClip(sound);
        audioSource.Play();
    }

    /// <summary>
    /// Play BGM
    /// </summary>
    public void PlayBGM()
    {
        audioSource.loop = false;
        Play(audioSource, Sound.BGM);
    }

    public void PlayFire()
    {
        audioSource.loop = false;
        Play(audioSource,Sound.Fire);
    }

    public void PlayDestroy()
    {
        audioSource.loop = false;
        Play(audioSource,Sound.Destroy);
    }

    private AudioClip GetAudioClip(Sound sound)
    {
        foreach (var soundClip in soundClips)
        {
            if (soundClip.Sound == sound)
            {
                return soundClip.AudioClip;
            }
        }
        Debug.Assert(false,$"cannot find sound {sound}");
        return null;
    }

    private void Awake()
    {
        Debug.Assert(audioSource != null,"audioSound cannot be null");
        Debug.Assert(soundClips != null && soundClips.Length != 0,"sound clips need to be set up");

        if (Instance == null)
        {
            Instance = this;
        }
    }
}
