﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{
    
    public class EnemyController : MonoBehaviour
    {
        private Transform playerTransform;
        private float enemyShipSpeed = 2;
        private float timeSearch = 2;
        [SerializeField] private float chasingThresholdDistance;
        [SerializeField] private EnemySpaceship enemySpaceship;
        private void Update()
        {
            MoveToPlayer();
        }

        private void MoveToPlayer()
        {
            if (playerTransform == null)
            {
                FindPlayer();
                return;
            }
            Vector3 enemyPosition = transform.position;
            Vector3 playerPosition = playerTransform.position;
            enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position
            
            Vector3 vectorToTarget = playerPosition - enemyPosition; // vector from this object towards the target location
            Vector3 directionToTarget = vectorToTarget.normalized;
            Vector3 velocity = directionToTarget * enemyShipSpeed;
                       
            float distanceToTarget = vectorToTarget.magnitude;

            if (distanceToTarget > chasingThresholdDistance)
            {
                transform.Translate(velocity * Time.deltaTime);
                enemySpaceship.Fire();
            }
            
        }

        private void FindPlayer()
        {
            if (timeSearch <= Time.time)
            {
                GameObject target = GameObject.FindWithTag("Player");
                if (target != null)
                {
                    playerTransform = target.transform;
                    timeSearch = Time.time + 0.5f;
                }
            }
        }
    }    
}

