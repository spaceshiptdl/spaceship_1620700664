﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawn : MonoBehaviour
{
    public GameObject enemyPrefabs;
    
    float spawnDistance = 10f;

    private float enemyRate = 5;

    private float nextEnemy = 1;

    void Update()
    {
        nextEnemy -= Time.deltaTime;

        if (nextEnemy <= 0)
        {
            nextEnemy = enemyRate;
            enemyRate *= 0.9f;
            if (enemyRate < 2)
            {
                enemyRate = 2;
            }

            Vector3 offset = Random.onUnitSphere;

            offset.z = 0;

            offset = offset.normalized * spawnDistance;

            Instantiate(enemyPrefabs, transform.position + offset, Quaternion.identity);
        }
    }
}
