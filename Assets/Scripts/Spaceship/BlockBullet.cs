﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockBullet : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            Destroy(other.gameObject);
        }
    }
}
