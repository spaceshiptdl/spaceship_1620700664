using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;
        private float fireRate = 2.5f;
        private float timeRespawn;
        private int damage = 1000;
        [SerializeField] private GameObject bulletPrefabs;
        [SerializeField] private GameObject rewardPrefabs;

        private void Start()
        {
            Hp = 100;
            Speed = 5;
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
            Instantiate(rewardPrefabs, transform.position, Quaternion.identity);
            ScoreManager.score += 1;
            SoundManager.Instance.PlayDestroy();
        }
        
        public override void Fire()
        {
            if (Time.time > timeRespawn)
            {
                timeRespawn = Time.time + fireRate;
                //Instantiate(bulletPrefabs, gunPosition.position, gunPosition.rotation);
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init();
            }
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == "Player")
            {
                var target = other.gameObject.GetComponent<IDamagable>();
                target?.TakeHit(damage);
                Destroy(gameObject);
            }
        }
    }
}