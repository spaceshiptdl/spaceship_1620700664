﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

public class Item : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            ScoreManager.score += 10;
            Destroy(gameObject);
        }
    }
}
